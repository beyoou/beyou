<div class="card">
    <div class="card-body">
        <h5 class="card-title fw-semibold mb-4">Id de Usuario</h5>
        <p class="mb-0"><?php echo $_SESSION['id']; ?></p>
        <h5 class="card-title fw-semibold mb-4">Nombre</h5>
        <p class="mb-0"><?php echo $_SESSION['first_name']; ?></p>
        <h5 class="card-title fw-semibold mb-4">Apellido</h5>
        <p class="mb-0"><?php echo $_SESSION['last_name']; ?></p>
        <h5 class="card-title fw-semibold mb-4">Correo</h5>
        <p class="mb-0"><?php echo $_SESSION['email']; ?></p>
    </div>
</div>