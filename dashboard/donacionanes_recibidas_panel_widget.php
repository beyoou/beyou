<?php
include '../layer3/layer3.php';

$id = $_SESSION["id"];
$sql = "SELECT SUM(donation) AS sum FROM donations WHERE id_receiver = $id";

$result = $conn->query($sql);

$row = $result->fetch_assoc();

$sum_donaciones = is_null($row['sum']) ? "0": $row['sum'];

$result->free();

$conn->close();
?>
<!-- Monthly Earnings -->
              <div class="col-lg-6">
                <div class="card">
                  <div class="card-body">
                    <div class="row alig n-items-start">
                      <div class="col-8">
                        <h5 class="card-title mb-9 fw-semibold"> Donaciones Recibidas </h5>
                        <h4 class="fw-semibold mb-3">$<?php echo $sum_donaciones; ?></h4>
                      </div>
                      <div class="col-4">
                        <div class="d-flex justify-content-end">
                          <div
                            class="text-white bg-secondary rounded-circle p-6 d-flex align-items-center justify-content-center">
                            <i class="ti ti-currency-dollar fs-6"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>