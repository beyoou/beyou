<?php
include '../layer3/layer3.php';

$id = $_SESSION["id"];
$sql = "SELECT COUNT(*) AS count FROM users WHERE id = $id AND category > 0 AND paid = 1";

$result = $conn->query($sql);

$row = $result->fetch_assoc();

$count_indicados = $row['count'];

$result->free();

$conn->close();
?>
<!-- Monthly Earnings -->
              <div class="col-lg-6">
                <div class="card">
                  <div class="card-body">
                    <div class="row alig n-items-start">
                      <div class="col-8">
                        <h5 class="card-title mb-9 fw-semibold"> Indicados </h5>
                        <h4 class="fw-semibold mb-3"><?php echo $count_indicados; ?></h4>
                      </div>
                      <div class="col-4">
                        <div class="d-flex justify-content-end">
                          <div
                            class="text-white bg-success rounded-circle p-6 d-flex align-items-center justify-content-center">
                            <i class="ti ti-user fs-6"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>