<?php
include '../layer3/layer3.php';

$id = $_SESSION["id"];

$sql = "SELECT u.first_name,u.last_name,d.donation FROM users u, donations d ";
$sql .= "WHERE u.invited_by = $id AND u.category > 0 AND u.paid = 1 ";
$sql .= "AND u.invited_by = $id AND u.id = d.id_donner "; 
$sql .= "ORDER BY u.category ASC";

$result = $conn->query($sql);

?>
        <div class="row">
          <div class="col-lg-12 d-flex align-items-stretch">  
            <div class="card w-100">
              <div class="card-body p-4">
                <h5 class="card-title fw-semibold mb-4">Donaciones</h5>
                <div class="table-responsive-xl">
                  <table class="table text-nowrap mb-0 align-middle">
                    <thead class="table-light text-dark fs-4">
                      <tr>
                        <th class="border-bottom-0">
                          <h6 class="fw-semibold mb-0">Nombre</h6>
                        </th>
                        <th class="border-bottom-0">
                          <h6 class="fw-semibold mb-0">Monto</h6>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      while ($row = $result->fetch_assoc()) {
                    ?>     
                      <tr>
                        <td class="border-bottom-0">
                            <h6 class="fw-semibold mb-1"><?php echo $row['first_name']." ".$row['last_name']; ?></h6>
                        </td>
                        <td class="border-bottom-0">
                          <div class="d-flex align-items-center gap-2">
                            <span class="badge bg-success rounded-3 fw-semibold"><?php echo $row['donation']; ?></span>
                          </div>
                        </td>
                      </tr>
                    <?php
                      }
                      
                      $result->free();

                      $conn->close();
                    ?>                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>