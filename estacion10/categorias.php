<?php
include '../layer3/layer3.php';

$sql = "SELECT * FROM categories LIMIT 3";

$result = $conn->query($sql);

while ($row = $result->fetch_assoc()) {

?>
<div class="col-md-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $row['category_description'];?></h5>
            <p class="card-text">Precio de la Categoria $<?php echo $row['ammount']; ?></p>
            <a href="seleccionar.php?categoria=<?php echo $row['id']; ?>" class="btn btn-primary">Seleccionar</a>
        </div>
    </div>
</div>
<?php
    }
                      
$result->free();

$conn->close();
?>    