<?php

require_once '../layer3/layer3.php';

$email = $_POST['email'];
$password = $_POST['password'];

$sql = "SELECT * FROM users WHERE email = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $email);
$stmt->execute();
$result = $stmt->get_result();

if($result->num_rows > 0){

    $row = $result->fetch_assoc();

    if($password == $row['password']){

        session_start();
        $_SESSION['id'] = $row['id'];
        $_SESSION['first_name'] = $row['first_name'];
        $_SESSION['last_name'] = $row['last_name'];
        $_SESSION['paid'] = $row['paid'];
        $_SESSION['email'] = $email;
        
        $conn->close();
        header("Location: ../dashboard/");
        exit();

    }
    else{

        $conn->close();
        $error_message = "Contraseña incorrecta.";
        header("Location: ../login/?error_message={$error_message}");
        exit();
    }
}
else{

    $conn->close();
    $error_message = "Fallo el intento de acceso.";
    header("Location: ../login/?error_message={$error_message}");
    exit();

}
?>