   <!-- Sidebar Start -->
   <aside class="left-sidebar">
      <!-- Sidebar scroll-->
      <div>
        <div class="brand-logo d-flex align-items-center justify-content-between">
          <a href="#" class="text-nowrap logo-img">
            <img valing="center" src="../assets/images/logos/new_logo.svg" alt=""/>
          </a>
          <div class="close-btn d-xl-none d-block sidebartoggler cursor-pointer" id="sidebarCollapse">
            <i class="ti ti-x fs-8"></i>
          </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav scroll-sidebar" data-simplebar="">
          <ul id="sidebarnav">
            <li class="nav-small-cap">
              <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
              <span class="hide-menu">INICIO</span>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="../dashboard/" aria-expanded="false">
                <span>
                  <i class="ti ti-dashboard"></i>
                </span>
                <span class="hide-menu">Panel</span>
              </a>
            </li>
            <li class="nav-small-cap">
              <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
              <span class="hide-menu">Estaciones</span>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="../estacion10/" aria-expanded="false">
                <span>
                  <i class="ti ti-flag"></i>
                </span>
                <span class="hide-menu">Estacion 10</span>
              </a>
            </li>
            <!--li class="sidebar-item">
              <a class="sidebar-link" href="#" aria-expanded="false">
                <span>
                  <i class="ti ti-bolt"></i>
                </span>
                <span class="hide-menu">Estacion 100</span>
              </a>
            </li-->
            <li class="nav-small-cap">
              <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
              <span class="hide-menu">EQUIPOS</span>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="../indicados/" aria-expanded="false">
                <span>
                  <i class="ti ti-user-plus"></i>
                </span>
                <span class="hide-menu">Indicados</span>
              </a>
            </li>
            <li class="nav-small-cap">
              <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
              <span class="hide-menu">Donaciones</span>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="../donaciones/" aria-expanded="false">
                <span>
                  <i class="ti ti-wallet"></i>
                </span>
                <span class="hide-menu">Recibidas</span>
              </a>
            </li>
            <li class="nav-small-cap">
              <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
              <span class="hide-menu">SOPORTE</span>
            </li>
            <!--li class="sidebar-item">
              <a class="sidebar-link" href="sample" aria-expanded="false">
                <span>
                  <i class="ti ti-file-description"></i>
                </span>
                <span class="hide-menu">F.A.Q.</span>
              </a>
            </li-->
            <li class="sidebar-item">
              <a class="sidebar-link" href="../support/" aria-expanded="false">
                <span>
                  <i class="ti ti-help"></i>
                </span>
                <span class="hide-menu">Soporte</span>
              </a>
            </li>
          </ul>
        </nav>
        <!-- End Sidebar navigation -->
      </div>
      <!-- End Sidebar scroll-->
    </aside>
    <!--  Sidebar End -->