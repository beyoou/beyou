<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Register</title>
  <link rel="shortcut icon" type="image/png" href="../assets/images/logos/favicon.png" />
  <link rel="stylesheet" href="../assets/css/styles.min.css" />
</head>

<body>
  <!--  Body Wrapper -->
  <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
    data-sidebar-position="fixed" data-header-position="fixed">
    <div
      class="position-relative overflow-hidden radial-gradient min-vh-100 d-flex align-items-center justify-content-center">
      <div class="d-flex align-items-center justify-content-center w-100">
        <div class="row justify-content-center w-100">
          <div class="col-md-8 col-lg-6 col-xxl-3">
            <div class="card mb-0">
              <div class="card-body">

                <?php if(isset($_GET["error_message"])){ ?>

                <div class="alert alert-warning" role="alert">
                    <?php echo $_GET["error_message"]; ?>
                </div>

                <?php } ?>

                <p class="text-center">Registrate</p>
                <form method="POST" action="register.php">
                  <div class="mb-3">
                    <label for="first_name" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" aria-describedby="textHelp" required minlength="3" maxlength="20" >
                  </div>
                  <div class="mb-3">
                    <label for="last_name" class="form-label">Apellido</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" aria-describedby="textHelp" required minlength="3" maxlength="20" >
                  </div>
                  <div class="mb-3">
                    <label for="invited_by" class="form-label">Invitado Por</label>
                    <input type="text" class="form-control" id="invited_by" name="invited_by" aria-describedby="textHelp" required minlength="1" maxlength="10" >
                  </div>
                  <div class="mb-3">
                    <label for="telephone" class="form-label">Telefono</label>
                    <input type="tel" class="form-control" id="telephone" name="telephone" aria-describedby="textHelp" required minlength="3" maxlength="15" >
                  </div>
                  <div class="mb-3">
                    <label for="email" class="form-label">Correo</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" required minlength="5" maxlength="60" >
                  </div>
                  <div class="mb-4">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password" required minlength="6" maxlength="20" >
                  </div>
                  <input type="submit" value="Registrate" class="btn btn-primary w-100 py-8 fs-4 mb-4 rounded-2">
                  <div class="d-flex align-items-center justify-content-center">
                    <p class="fs-4 mb-0 fw-bold">Ya tiene cuenta?</p>
                    <a class="text-primary fw-bold ms-2" href="../login/">Ingresa</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
  <script src="../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>