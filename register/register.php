<?php
require_once '../layer3/layer3.php';

$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$telephone = $_POST['telephone'];
$invited_by = $_POST['invited_by'];
$email = $_POST['email'];
$password = $_POST['password'];

$sql = "INSERT INTO users (first_name, last_name, telephone, invited_by, email, password) ";
$sql .= "VALUES (?, ?, ?, ?, ?, ?)";

$stmt = $conn->prepare($sql);

if($stmt ){

    try{

        $stmt->bind_param("ssssss", $first_name, $last_name, $telephone, $invited_by, $email, $password);
        $stmt->execute();
        $stmt->close();
        $conn->close();

        $error_message = "Te registraste con exito, ya puedes ingresar";
        header("Location: ../login/?error_message={$error_message }");
        exit();

    }
    catch(Exception $e){

        $stmt->close();
        $conn->close();
        $error_message = $e->getMessage();
        header("Location: ../register/?error_message={$error_message}");
        exit();

    }

}
else{
    $conn->close();
    $error_message = "Fallo el registro contacte a soporte.";
    header("Location: ../register/?error_message={$error_message}");
    exit();
}
?>