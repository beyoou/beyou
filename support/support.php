<div class="card">
    <div class="card-body">
        <h5 class="card-title fw-semibold mb-4">Contacto con Soporte</h5>
        <p class="mb-0">Para contactar con soporte favor escriba un correo a: <a href="mailto:soporte@divinaluz.net">soporte@divinaluz.net</a></p>
    </div>
</div>