<?php 
session_start();

if(!isset($_SESSION["id"]))
{
  $error_message = "No ha iniciado sesion";
  header("Location: ../login/?error_message={$error_message}");
  exit();
}

?>